<?php

namespace App\Repository;

use App\Entity\IngredientPrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IngredientPrice|null find($id, $lockMode = null, $lockVersion = null)
 * @method IngredientPrice|null findOneBy(array $criteria, array $orderBy = null)
 * @method IngredientPrice[]    findAll()
 * @method IngredientPrice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IngredientPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IngredientPrice::class);
    }

    // /**
    //  * @return IngredientPrice[] Returns an array of IngredientPrice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IngredientPrice
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
