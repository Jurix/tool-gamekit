<?php

namespace App\Repository;

use App\Entity\PromotedGameProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PromotedGameProfile|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromotedGameProfile|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromotedGameProfile[]    findAll()
 * @method PromotedGameProfile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromotedGameProfileRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PromotedGameProfile::class);
    }

    public function returnGamesWithoutProfile() {
        $connection = $this->getEntityManager()->getConnection();
        $sql = 'SELECT pg.game_name, pg.country_code FROM `promoted_games` AS pg GROUP BY pg.game_name, pg.country_code';
        $statement = $connection->prepare($sql);
        $statement->execute();
        return $statement->fetchAll();
    }

    // /**
    //  * @return PromotedGameProfile[] Returns an array of PromotedGameProfile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromotedGameProfile
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
