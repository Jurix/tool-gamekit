<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromotedGameProfileRepository")
 */
class PromotedGameProfile
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $GameName;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $Language;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $Pictures;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $Forum;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $News;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $Description;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $Avatar;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGameName(): ?string
    {
        return $this->GameName;
    }

    public function setGameName(string $GameName): self
    {
        $this->GameName = $GameName;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->Language;
    }

    public function setLanguage(string $Language): self
    {
        $this->Language = $Language;

        return $this;
    }

    public function getPictures(): ?bool
    {
        return $this->Pictures;
    }

    public function setPictures(bool $Pictures): self
    {
        $this->Pictures = $Pictures;

        return $this;
    }

    public function getForum(): ?bool
    {
        return $this->Forum;
    }

    public function setForum(bool $Forum): self
    {
        $this->Forum = $Forum;

        return $this;
    }

    public function getNews(): ?bool
    {
        return $this->News;
    }

    public function setNews(bool $News): self
    {
        $this->News = $News;

        return $this;
    }

    public function getDescription(): ?bool
    {
        return $this->Description;
    }

    public function setDescription(bool $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getAvatar(): ?bool
    {
        return $this->Avatar;
    }

    public function setAvatar(bool $Avatar): self
    {
        $this->Avatar = $Avatar;

        return $this;
    }
}
