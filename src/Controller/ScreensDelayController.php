<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ScreensDelay;
use Symfony\Component\HttpFoundation\Response;
use App\Services\GamekitScreenApi;
use App\Repository\ScreensDelayRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ScreensDelayAvgRangeType;

class ScreensDelayController extends AbstractController
{
    /**
     * @Route("/screens", name="screens_delay_log")
     */
    public function index()
    {

        return $this->render('screens_delay/index.html.twig', [
            
        ]);
    }

    /**
     * @Route("/screens/avg", name="screens_delay_avg")
     */
    public function avgScreenDelay(ScreensDelayRepository $ScreensDelayRepository, Request $request)
    {
        $delay['daily'] = $ScreensDelayRepository->returnAvgDelayDaily();
        $delay['monthly'] = $ScreensDelayRepository->returnAvgDelayMonthly();

        $form = $this->createForm(ScreensDelayAvgRangeType::class, [
            'action' => $this->generateUrl('screens_delay_avg'),
            'method' => 'POST'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) { //to fix
            $data = $request->request->get('screens_delay_avg_range');

            $dateStart = $data['date_start']['year'].'-'.str_pad($data['date_start']['month'], 2, '0', STR_PAD_LEFT).'-'.str_pad($data['date_start']['day'], 2, '0', STR_PAD_LEFT);
            $dateStop = $data['date_stop']['year'].'-'.str_pad($data['date_stop']['month'], 2, '0', STR_PAD_LEFT).'-'.str_pad($data['date_stop']['day'], 2, '0', STR_PAD_LEFT);

            return $this->redirectToRoute('screens_delay_avg_in_range', ['dateStart' => $dateStart, 'dateStop' => $dateStop]);
        }

        return $this->render('screens_delay/avg.html.twig', [
            'delay' => $delay,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/screens/avg/{dateStart}/{dateStop}/", name="screens_delay_avg_in_range")
     */
    public function avgScreenDelayInRange(ScreensDelayRepository $ScreensDelayRepository, $dateStart, $dateStop)
    {
        if(!preg_match('@^\d{4}-\d{1,2}-\d{1,2}$@', $dateStart) || !preg_match('@^\d{4}-\d{1,2}-\d{1,2}$@', $dateStop)) {
            throw new Exception('You have to put date in format YYYY-MM-dd');
        }

        $delay['daily'] = $ScreensDelayRepository->returnAvgDelayDailyInRange($dateStart, $dateStop);
        $delay['total'] = $ScreensDelayRepository->returnAvgDelayInRange($dateStart, $dateStop);
        return $this->render('screens_delay/avgInRange.html.twig', [
            'delay' => $delay
        ]);
    }

    /**
     * @Route("/screens/api", name="screens_delay_api")
     */
    public function insertDelayToDatabase()
    {
    	$ApiFeedback = new GamekitScreenApi;

    	$records = new ScreensDelay();
    	$records->setDGScreens($ApiFeedback->DogryNumberOfScreens);
    	$records->setDGTime($ApiFeedback->DogryDateDelay);
    	$records->setGKScreens($ApiFeedback->GamekitNumberOfScreens);
    	$records->setGKTime($ApiFeedback->GamekitDateDelay);
    	$records->setTimeStat($ApiFeedback->dateNow);

    	$em = $this->getDoctrine()->getManager();
    	$em->persist($records);
    	$em->flush();

    	return new Response();
    }
}
