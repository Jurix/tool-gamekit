<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Food;
use App\Entity\Size;
use App\Form\CategoryType;
use App\Form\FoodType;
use App\Form\SizeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends AbstractController
{
    /**
     * @Route("/admin/menu", name="admin_menu")
     */
    public function index(Request $request)
    {
        $foodForms = null;
        $foodFromCategory = null;
        $sizeForms = null;
        $sizesInCategory = null;

        $foodCategories = $this->getDoctrine()->getRepository(Category::class)->findBy(['type' => 1]);
        foreach ($foodCategories as $foodCategory) {
            $food = new Food();
            $id = $foodCategory->getId();
            $foodForms[$id] = $this->createForm(FoodType::class, $food, [
                'action' => $this->generateUrl('admin_food_add', ['categoryId' => $id]),
                'method' => 'POST'
            ])->createView();
            $foodFromCategory[$id] = $this->getDoctrine()->getRepository(Food::class)->findBy(['category' => $foodCategory], ['listOrder' => 'ASC']);

            $size = new Size();
            $sizeForms[$id]['new'] = $this->createForm(SizeType::class, $size, [
                'action' => $this->generateUrl('admin_size_add', ['categoryId' => $id]),
                'method' => 'POST'
            ])->createView();

            $sizesInCategory = $this->getDoctrine()->getRepository(Size::class)->findBy(['category' => $foodCategory]);
            foreach ($sizesInCategory as $sizeInCategory) {
                $sizeForms[$id][] = $this->createForm(SizeType::class, $sizeInCategory, [
                    'action' => $this->generateUrl('admin_size_edit', ['sizeId' => $sizeInCategory->getId()]),
                    'method' => 'POST'
                ])->createView();
            }
        }

        $category = new Category();
        $formCategory = $this->createForm(CategoryType::class, $category);
        $formCategory->handleRequest($request);
        if ($formCategory->isSubmitted() && $formCategory->isValid()) {
            $category->setType(1);
            $category->setListOrder(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute("admin_menu");
        }

        return $this->render('admin/menu/index.html.twig', [
            'formCategory' => $formCategory->createView(),
            'foodCategories' => $foodCategories,
            'foodForms' => $foodForms,
            'foodFromCategory' => $foodFromCategory,
            'sizeForms' => $sizeForms,
            'sizesInCategory' => $sizesInCategory,
        ]);
    }

    /**
     * @Route("/admin/food/add/{categoryId}", name="admin_food_add")
     */
    public function addFood(Request $request, int $categoryId)
    {
        $food = new Food();
        $form = $this->createForm(FoodType::class, $food);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['id' => $categoryId]);
            $food->setCategory($category);
            $em = $this->getDoctrine()->getManager();
            $em->persist($food);
            $em->flush();
        }

        return $this->redirectToRoute("admin_menu");
    }

    /**
     * @Route("/admin/size/add/{categoryId}", name="admin_size_add")
     */
    public function addSize(Request $request, int $categoryId)
    {
        $size = new Size();
        $form = $this->createForm(SizeType::class, $size);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['id' => $categoryId]);
            $size->setCategory($category);
            $em = $this->getDoctrine()->getManager();
            $em->persist($size);
            $em->flush();
        }

        return $this->redirectToRoute("admin_menu");
    }

    /**
     * @Route("/admin/size/edit/{sizeId}", name="admin_size_edit")
     */
    public function editSize(Request $request, int $sizeId)
    {
        $size = new Size();
        $form = $this->createForm(SizeType::class, $size);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $size = $this->getDoctrine()->getRepository(Size::class)->findOneBy(['id' => $sizeId]);
            $size->setSize($form->getData()->getSize());
            $em = $this->getDoctrine()->getManager();
            $em->persist($size);
            $em->flush();
        }

        return $this->redirectToRoute("admin_menu");
    }
}
