<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Ingredient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/admin/category/delete/{categoryId}", name="admin_category_delete")
     */
    public function deleteCategory(int $categoryId)
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['id' => $categoryId]);
        $type = $category->getType();

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        if ($type === 1) {
            return $this->redirectToRoute("admin_menu");
        } elseif ($type === 2) {
            return $this->redirectToRoute("admin_ingredient");
        }

        return $this->redirectToRoute("admin");
    }
}
