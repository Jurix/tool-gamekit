<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Ingredient;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\IngredientType;


class IngredientController extends AbstractController
{
    /**
     * @Route("/admin/ingredient", name="admin_ingredient")
     */
    public function index(Request $request)
    {
        $ingredientForms = null;
        $ingredientsFromCategory = null;

        $ingredientCategories = $this->getDoctrine()->getRepository(Category::class)->findBy(['type' => 2]);
        foreach ($ingredientCategories as $ingredientCategory) {
            $ingredient = new Ingredient();
            $id = $ingredientCategory->getId();
            $ingredientForms[$id] = $this->createForm(IngredientType::class, $ingredient, [
                'action' => $this->generateUrl('admin_ingredient_add', ['categoryId' => $id]),
                'method' => 'POST'
            ])->createView();
            $ingredientsFromCategory[$id] = $this->getDoctrine()->getRepository(Ingredient::class)->findBy(['category' => $ingredientCategory], ['listOrder' => 'ASC']);
        }

        $category = new Category();
        $formCategory = $this->createForm(CategoryType::class, $category);
        $formCategory->handleRequest($request);
        if ($formCategory->isSubmitted() && $formCategory->isValid()) {
            $category->setType(2);
            $category->setListOrder(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute("admin_ingredient");
        }

        return $this->render('admin/ingredient/index.html.twig', [
            'ingredientCategories' => $ingredientCategories,
            'ingredientForms' => $ingredientForms,
            'ingredientsFromCategory' => $ingredientsFromCategory,
            'formCategory' => $formCategory->createView(),
        ]);
    }

    /**
     * @Route("/admin/ingredient/add/{categoryId}", name="admin_ingredient_add")
     */
    public function addIngredient(Request $request, int $categoryId)
    {
        $ingredient = new Ingredient();
        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['id' => $categoryId]);
            $ingredient->setCategory($category);
            $em = $this->getDoctrine()->getManager();
            $em->persist($ingredient);
            $em->flush();
        }

        return $this->redirectToRoute("admin_ingredient");
    }

    /**
     * @Route("/admin/ingredient/delete/{ingredientId}", name="admin_ingredient_delete")
     */
    public function deleteIngredient(int $ingredientId)
    {
        $ingredient = $this->getDoctrine()->getRepository(Ingredient::class)->findOneBy(['id' => $ingredientId]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($ingredient);
        $em->flush();

        return $this->redirectToRoute("admin_ingredient");
    }
}
