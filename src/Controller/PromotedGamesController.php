<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\PromotedGamesImportType;
use Symfony\Component\HttpFoundation\Request;
use App\Services\PromotedGamesCSVManagement;
use App\Services\PromotedGamesView;
use App\Entity\PromotedGames;
use App\Repository\PromotedGamesRepository;
use App\Form\GamesListMonthSelectType;
use App\Repository\PromotedGamesProfilesRepository;
use App\Services\PromotedGamesProfilesManagement;
use App\Entity\PromotedGameProfile;
use App\Form\ProfileManagementType;
use App\Form\ProfilesManagementType;
use App\Repository\PromotedGameProfileRepository;
use Symfony\Component\HttpFoundation\Response;

class PromotedGamesController extends AbstractController
{
    /**
     * @Route("/games/upload", name="promoted_games_upload")
     */
    public function upload(Request $request)
    {
    	$form = $this->createForm(PromotedGamesImportType::class, [
            'action' => $this->generateUrl('promoted_games_upload'),
            'method' => 'POST'
        ]);
        $feedback = null;
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = fopen($request->files->get('promoted_games_import')['file']->getRealPath(), 'r');
			$em = $this->getDoctrine()->getManager();
    		$csv = new PromotedGamesCSVManagement($file, $em);
            $feedback = $csv->feedback;
    	}

        return $this->render('promoted_games/upload.html.twig', [
            'form' => $form->createView(),
            'feedback' => $feedback
        ]);
    }
    /**
     * @Route("/games", name="promoted_games_news_form")
     */
    public function printGamesListForm(Request $request)
    {
        $form = $this->createForm(GamesListMonthSelectType::class, [
            'action' => $this->generateUrl('promoted_games_news_form'),
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $date = $request->request->get('games_list_month_select')['raport_date'];
            $year = $date['year'];
            $month = $date['month'];
            
            return $this->redirectToRoute('promoted_games_news', ['month' => $month, 'year' => $year]);
        }

        return $this->render('promoted_games/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/games/{year}/{month}", name="promoted_games_news", requirements={"year"="\d+"})
     */
    public function printGamesListInMonth(PromotedGamesRepository $PromotedGamesRepository, Request $request, $year, $month)
    {
    	$games = $PromotedGamesRepository->returnGamesListInMonth($year, $month);

    	$view = new PromotedGamesView($games);

    	return $this->render('promoted_games/news.html.twig', [
            'games_table' => $view->printGames()
        ]);
    }
    /**
     * @Route("/games/profiles", name="promoted_games_profiles_selection")
     */
    public function gameProfileSelection(Request $request)
    {
        
        return $this->render('promoted_games/profileSelect.html.twig');
    }



    /**
     * @Route("/games/profiles/{section}", name="promoted_games_profiles_form"), requirements={"section"="{2}"})
     */
    public function printGamesProfilesForm(Request $request, PromotedGameProfileRepository $pgpRepo, $section)
    {

        $form = $this->createForm(ProfilesManagementType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()) {
            $profiles = $form->getData()['profile'];
            $em = $this->getDoctrine()->getManager();
            foreach ($profiles as $key => $profile) {
                $profileDB = $this->getDoctrine()->getRepository(PromotedGameProfile::class)->findOneBy(['id' => $key]);
                $profileDB->setPictures(!empty($profile->getPictures()));
                $profileDB->setForum(!empty($profile->getForum()));
                $profileDB->setNews(!empty($profile->getNews()));
                $profileDB->setDescription(!empty($profile->getDescription()));
                $profileDB->setAvatar(!empty($profile->getAvatar()));
                $em->persist($profileDB);
            }
            $em->flush();
        }

        $pgv = new PromotedGamesView([]);
        $language = $pgv->returnSection($section);
        $profiles = $pgpRepo->findBy(['Language' => $language], ['Language' => 'ASC', 'GameName' => 'ASC']);
        $serializer = $this->container->get('serializer'); 
        $json = $serializer->serialize($profiles,'json');

        $pgp = new PromotedGameProfile();

        return $this->render('promoted_games/profiles.html.twig', [
            'form' => $form->createView(),
            'json' => $json,
        ]);
    }

    /**
     * @Route("/games/refresh", name="promoted_games_profiles_add")
     */
    public function addProfileToNewGames(Request $request, PromotedGameProfileRepository $pgpRepo) {
        $games = $pgpRepo->returnGamesWithoutProfile();
        $pgpm = new PromotedGamesProfilesManagement();
        $profiles = $pgpm->returnProfileListFromGameList($games);

        $em = $this->getDoctrine()->getManager();
        foreach ($profiles as $profile) {
            $profilesInDb = $pgpRepo->findBy(
                ['GameName' => $profile['game_name']]
            );
            if(count($profilesInDb) > 0) {
                continue;
            }

            $gameProfile = new PromotedGameProfile();
            $gameProfile->setPictures(0);
            $gameProfile->setForum(0);
            $gameProfile->setNews(0);
            $gameProfile->setDescription(0);
            $gameProfile->setAvatar(0);
            $gameProfile->setGameName($profile['game_name']);
            $gameProfile->setLanguage($profile['language']);

            $em->persist($gameProfile);
        }
        $em->flush();
        $em->clear();
        return $this->redirectToRoute('promoted_games_profiles_selection');
    }
}
