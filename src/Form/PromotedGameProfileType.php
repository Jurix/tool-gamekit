<?php

namespace App\Form;

use App\Entity\PromotedGameProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class PromotedGameProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('GameName', TextType::class, ['attr' => ['disabled' => true]])
            ->add('Language', HiddenType::class, ['attr' => ['disabled' => true]])
            ->add('Pictures')
            ->add('Forum')
            ->add('News')
            ->add('Description')
            ->add('Avatar')
            ->add('debug', HiddenType::class, ['data' => 'debug', 'mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PromotedGameProfile::class, 
        ]);
    }
}
