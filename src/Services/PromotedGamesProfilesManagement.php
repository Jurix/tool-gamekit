<?php
namespace App\Services;

use App\Entity\PromotedGames;
use App\Services\PromotedGamesView;

class PromotedGamesProfilesManagement {
	public function returnProfileListFromGameList($games) {
        $pgv = new PromotedGamesView($games);
		$profiles = array();
        foreach ($games as $game) {
            $language = $pgv->returnSection($game['country_code']);  
            unset($game['country_code']);
            $game['language'] = $language;
            if(in_array($game, $profiles)) {
                continue;
        	}
        $profiles[] = $game; 
        }
        return $profiles;
	} 
}